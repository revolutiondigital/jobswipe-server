from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
admin.autodiscover()

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
	(r'^admin/', include(admin.site.urls)),
	 #USERINFO URLS
	 #user signup
	  url(r'^userinfo/usersignup$', 'userinfo.views.user_signup'),
      #check facebook token to see if it already exists
      url(r'^userinfo/checkfacebooktoken$', 'userinfo.views.check_facebook_token'),
      #get user role to know if they are employer or employee
      url(r'^userinfo/getuserdata$', 'userinfo.views.get_user_data'),
      #choose user type(i.e slide for employee/employer)
      url(r'^userinfo/chooseusertype$', 'userinfo.views.choose_user_type'),
      #user first step configuration
      url(r'^userinfo/employeefirststep$', 'userinfo.views.employee_first_step'),
      url(r'^userinfo/employerfirststep$', 'userinfo.views.employer_first_step'),
      #changeprofilepicture(need ot figure out cropping)
      url(r'^userinfo/changeprofilepicture$', 'userinfo.views.change_profile_picture'),
      url(r'^userinfo/getquickbloxsession$', 'userinfo.views.get_quickblox_session'),
      url(r'^userinfo/setquickbloxid$', 'userinfo.views.set_quickblox_id'),
      url(r'^userinfo/getemployerprofiledata$', 'userinfo.views.get_employer_profile_data'),
      url(r'^userinfo/getemployeeprofiledata$', 'userinfo.views.get_employee_profile_data'),
      url(r'^userinfo/updatelocation$', 'userinfo.views.update_location'),
      url(r'^userinfo/editemployerprofile$', 'userinfo.views.edit_employer_data'),
      url(r'^userinfo/croppicture$', 'userinfo.views.crop_picture'),
      url(r'^userinfo/editemployeeprofile$', 'userinfo.views.edit_employee_data'),
      url(r'^userinfo/updateemployeeprofile$', 'userinfo.views.update_employee_data'),
      url(r'^userinfo/deleteemployeeprofile$', 'userinfo.views.delete_employee_data'),
      url(r'^userinfo/getuserdiscoverypreferences$', 'userinfo.views.get_user_discovery_preferences'),
      url(r'^userinfo/updateuserdiscoverypreferences$', 'userinfo.views.update_user_discovery_preferences'),
      url(r'^userinfo/getnewnotifications$', 'userinfo.views.get_new_notifications'),
      url(r'^userinfo/updatenewnotifications$', 'userinfo.views.update_new_notifications'),
      url(r'^userinfo/updateaddress$', 'userinfo.views.update_address'),
      url(r'^userinfo/requestcheque$', 'userinfo.views.request_cheque'),
      url(r'^userinfo/hasusertype$', 'userinfo.views.has_user_type'),
      url(r'^userinfo/setemail$', 'userinfo.views.set_email'),
      url(r'^login$', 'userinfo.views.user_login'),
    #JOB URLS
      url(r'^jobs/getjobclasses$', 'jobs.views.get_job_classes'),
      url(r'^jobs/createjob$', 'jobs.views.create_job'),
      url(r'^jobs/getemployerjobs$', 'jobs.views.get_employer_jobs'),
      #url(r'^jobs/employergetjobemployees$', 'jobs.views.get_job_employees'),
      url(r'^jobs/getjobinfo$', 'jobs.views.get_job_info'),
      url(r'^jobs/getemployeesswipedright$', 'jobs.views.get_employees_swiped_right'),
      url(r'^jobs/employerselection$', 'jobs.views.employer_selection'),
      url(r'^jobs/employeeselection$', 'jobs.views.employee_selection'),
    url(r'^jobs/getjobsinarea$', 'jobs.views.get_jobs_in_area'),
    url(r'^jobs/getjobdetails$', 'jobs.views.get_job_details'),
    url(r'^jobs/editjob$', 'jobs.views.edit_job'),
    url(r'^jobs/deletejob$', 'jobs.views.delete_job'),
    url(r'^jobs/offermatch$', 'jobs.views.offer_match'),
    url(r'^jobs/removematch$', 'jobs.views.remove_match'),
    url(r'^jobs/acceptoffer$', 'jobs.views.accept_offer'),
    url(r'^jobs/getmatches$', 'jobs.views.get_matches'),
    url(r'^jobs/completejob$', 'jobs.views.complete_job'),
    url(r'^addscrapedjobs$', 'jobs.views.add_scraped_jobs'),
    url(r'^addscrapedjobsspreadsheet$', 'jobs.views.add_scraped_jobs_spreadsheet'),

    #messages stuff
    url(r'^messages/getmatchmessages$', 'messages.views.get_match_messages'),
    url(r'^messages/getchatroom$', 'messages.views.get_chatroom'),
    url(r'^messages/setchatroom$', 'messages.views.set_chatroom'),
    url(r'^messages/updatechatroom$', 'messages.views.update_chatroom'),
    url(r'^messages/markasread$', 'messages.views.mark_as_read'),
    url(r'^messages/getjobschatroom$', 'messages.views.get_jobs_chatroom'),

    # Examples:
    # url(r'^$', 'martek.views.home', name='home'),
    # url(r'^martek/', include('martek.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
