from django.db import models
from django.utils import timezone
# Create your models here.
class JobType(models.Model):
	name = models.CharField(max_length=100)


class Job(models.Model):
	posted_by = models.ForeignKey('userinfo.UserInfo')
	title = models.CharField(max_length=100)
	wage = models.DecimalField(max_digits=10, decimal_places=2)
	description = models.CharField(max_length =1000,)
	job_type = models.ForeignKey(JobType)
	latitude = models.DecimalField(max_digits=11, decimal_places=7)
	longitude = models.DecimalField(max_digits=11, decimal_places=7)
	is_approved = models.BooleanField(default = True)
	is_filled = models.BooleanField(default = False)
	is_completed = models.BooleanField(default= False)
	use_address = models.BooleanField(default = False)
	num_applicants = models.PositiveIntegerField(default=0)
	num_flagged = models.PositiveIntegerField(default=0)
	date_created = models.DateTimeField(auto_now_add=True)
	job_image_url = models.CharField(max_length = 500)
	#fields needed if scraped
	is_scraped = models.BooleanField(default =False)
	email = models.EmailField(max_length=254)
	job_url = models.CharField(max_length=500, default=False)
	scraped_from = models.CharField(max_length=50, default= False)
	#fields neeeded if guarantee pay
	is_guarantee_pay = models.BooleanField(default = False)


class EmployeeChoice(models.Model):
	employee= models.ForeignKey('userinfo.UserInfo')
	job = models.ForeignKey(Job)
	#is_chosen is true is user has swiper right
	is_chosen = models.BooleanField()
	#is viewed is true if employer has seen the swipe right
	is_viewed = models.BooleanField(default =False)

class EmployerChoice(models.Model):
	employer = models.ForeignKey('userinfo.UserInfo', related_name="employerchoice.employer")
	employee = models.ForeignKey('userinfo.UserInfo', related_name= "employerchoice.employee")
	job = models.ForeignKey(Job)
	is_chosen = models.BooleanField(default =False)
	is_viewed= models.BooleanField(default =False)
	job = models.ForeignKey(Job)
	is_chosen = models.BooleanField(default =False)
	is_viewed= models.BooleanField(default =False)

class JobMatch(models.Model):
    employer = models.ForeignKey('userinfo.UserInfo',related_name= "jobmatch.employer")
    employee = models.ForeignKey('userinfo.UserInfo', related_name="jobmatch.employee")
    job = models.ForeignKey(Job)
    #true if there has been a new message
    is_new_employer = models.BooleanField(default = True)
    is_new_employee = models.BooleanField(default = True)
    is_unmatched = models.BooleanField(default= False)
    date_added = models.DateTimeField(default = timezone.now())
    last_message_text = models.CharField(max_length= 200,default="")
    last_message_sent = models.DateTimeField(auto_now = True)
    chatroom_id = models.CharField(max_length=100,blank = True, null = True)
    is_hired = models.BooleanField(default = False)
    is_offered = models.BooleanField(default= False)
    #true if there has been a new job offer etc
    last_updated = models.DateTimeField(default=timezone.now(),auto_now_add=timezone.now(),auto_now=True)

#class to use if user wants to attach address to the job
class JobAddress(models.Model):
	job = models.ForeignKey(Job)
	address = models.CharField(max_length=200)
	city = models.CharField(max_length = 200)
	region = models.CharField(max_length=100)
	country = models.CharField(max_length = 200)



