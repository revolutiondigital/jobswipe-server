from django.contrib import admin
from jobs.models import *

class JobAdmin(admin.ModelAdmin ):
    # ...
    list_display = ('title','wage','email','is_scraped')
admin.site.register(Job, JobAdmin)
