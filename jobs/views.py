# Create your views here.
from decimal import *
import os
import datetime
from django.conf import settings
from django.shortcuts import render_to_response, render
from django.template.defaulttags import csrf_token
from django.template import RequestContext
from django.contrib.auth.models import User,Group
from django.contrib.auth import *
from django.http import  *
from django.conf import settings
from django.middleware.csrf import get_token
import json
import ast
from userinfo.models import *
from jobs.models import *
from messages.models import *
from userinfo.views import get_user
from django.views.decorators.http import *
from django.contrib.auth.decorators import login_required
import urllib2
from django.views.decorators.csrf import *
from decimal import Decimal
from django.db.models import *
from random import randint
import stripe
import random
from django.db.models import Q
import mandrill
import csv
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
import math
#FUNCTION used to get all the different job classes
@require_GET
def get_job_classes(request):
	job_classes = JobType.objects.filter().order_by('name')
	return_data = []
	for item in job_classes:
		new_item = {}
		new_item['name'] = item.name
		new_item['id'] = item.id
		return_data.append(new_item)
	return HttpResponse(json.dumps(return_data))


#FUNCTION used to create a job
#input: job:{latitude,longitude,description,title,pay}, useJobAddress, socialUserID,accessToken
@require_POST
@csrf_exempt
def create_job(request):
	#get all the data required to post job
	try:
		post_data= json.loads(request.body)
		job = post_data.get('job')
		use_job_address = post_data.get('useJobAddress')
		user_id = post_data.get('socialUserID')
		match_id  = post_data.get('matchID')
		access_token= post_data.get('accessToken')
		is_guarantee_pay = post_data.get('isGuaranteePay')
		use_address= post_data.get('useAddress')
		print access_token
	except:
		return HttpResponseBadRequest()
	#if we have made a guarantee pay job

	if job.get('description') == "" or job.get('description') == None:
		job['description'] = ""

	user = get_user(user_id, access_token)
	if user == None:
		return HttpResponseServerError()
	#if guarantee pay is true, we change the card
	if is_guarantee_pay == True:
	    is_guarantee_pay = True
	    #we need to charge card
	    stripe_token = post_data.get('stripeToken')
	    stripe.api_key = "sk_test_xnsZ7X61v23hsSL5L4k55W7L"
	    #amount in cents
	    amount = job.get('pay')*100
	    try:
		change = stripe.Charge.create(
			amount= amount,
			currency="cad",
			card=stripe_token,
			description="Guarantee Job payment by" +user.first_name + " " + user.last_name
			)
	    except:
		print "Credit card fail"
		return HttpResponseServerError('Your Credit Card has Failed')

	else:
	    is_guarantee_pay = False



	#check to see if we are using address
	if use_address == True:
	    address_data= post_data.get('address')
	    address = address_data.get('address')
	    city = address_data.get('city')
	    region = address_data.get('region')
	    country = address_data.get('country')
	    #if address hasn't been set yet, lets set itor is different then we change it
	    if user.address != address:
		user.address= address
		user.city = city
		user.region = region
		user.country = country
		user.save()

	#check to see if user has already created a job with that title
	try:
		job = Job.objects.get(posted_by = user, title= job.get('title'))
		return HttpResponseServerError('This Job Already Exists')
	except:
		pass
	#get job type for job
	try:
		job_type = JobType.objects.get(id = job.get('jobClassSelected')	)
	except Exception, e:
		print e
		return HttpResponseServerError()
	#create new job
	latitude = Decimal(job.get('latitude').strip(' "'))
	longitude = Decimal(job.get('longitude').strip(' "'))

	job_new = Job(posted_by = user,
			title= job.get('title'),
			wage =job.get('pay'),
			description =job.get('description'),
			job_type = job_type,
			latitude = latitude,
			longitude = longitude,
			job_image_url = user.profile_picture_url,
			is_guarantee_pay= is_guarantee_pay
				)

	job_new.save()
	return HttpResponse(job_new.id)

#Function used to get all employer jobs
@require_GET
def get_employer_jobs(request):
	#Get user data
	try:
		user_id = request.GET.get('socialUserID')
		access_token = request.GET.get('accessToken')
	except:
		return HttpResponseBadRequest()

	print user_id
	print access_token

	user = get_user(user_id, access_token)
	print user
	if user == None:
		return HttpResponseServerError()
	#get all the jobs that the user has posted that haven't been filled
	try:
		jobs = Job.objects.filter(posted_by = user, is_completed = False).order_by('-date_created')
	except:
		return HttpResponseServerError()
	return_jobs =[]
	for job in jobs:
		new_job ={}
		new_job['wage'] = str(job.wage)
		new_job['id'] = job.id
		new_job['title'] = job.title
		new_job['description'] = job.description
		new_job['isGuaranteePay'] = job.is_guarantee_pay
		#get num of new applicants
		try:
		    num_new_applicants = EmployeeChoice.objects.filter(job = job, is_chosen = True, is_viewed=False).count()
		except:
		    print "numapplicantsfailed"
		    num_new_applicants = 0
		new_job['num_new_applicants'] =num_new_applicants
		return_jobs.append(new_job)
	return HttpResponse(json.dumps(return_jobs))


#FUNCTION get job details based on job id
def get_job_info(request):
	try:
		job_id = request.GET.get('jobID')
		user_id = request.GET.get('socialUserID')
		access_token = request.GET.get('accessToken')
	except:
		return HttpResponseBadRequest()
	#get user
	user = get_user(user_id,access_token)
	if user is None:
		return HttpResponseForbidden()
	#get job details to send back to user
	print job_id
	try:
		job = Job.objects.get(id = job_id, posted_by = user)
	except:
		return HttpResponseServerError()
	#prepare return job data
	return_job = {}
	return_job['id'] = job.id
	return_job['title'] = job.title
	return_job['is_scaped'] = job.is_scraped
	return_job['job_image_url'] = job.job_image_url
	return_job['description'] = job.description
	#return data back to user
	return HttpResponse(json.dumps(return_job))

#Function get employess that have swiped right to job
#input: job_id
#output: [employees]
def get_employees_swiped_right(request):
	try:
		job_id  = request.GET.get('jobID')
		user_id = request.GET.get('socialUserID')
		access_token = request.GET.get('accessToken')
	except:
		return HttpResponseBadRequest()
	#get employer
	user= get_user(user_id,access_token)
	if user is None:
		return HttpResponseForbidden()
	#get job

	try:
		job = Job.objects.get(id = job_id,posted_by = user)
	except:
		return HttpResponseServerError()
	#get all employees that have swiped right to the job ad havent been viewed yet
	try:
		employee_choices = EmployeeChoice.objects.filter(job = job, is_chosen= True, is_viewed= False )
	except:
		return HttpResponseServerError()

	#employees to return
	return_employees = []
	#iterate through all employees to create return data
	for employee_choice in employee_choices:
		new_employee= {}
		employee = employee_choice.employee
		new_employee['id'] = employee.id
		new_employee['firstName'] = employee.first_name
		new_employee['lastName'] = employee.last_name
		new_employee['profilePictureURL'] = employee.profile_picture_url
		#get employee gender
		if employee.sex == "male":
			new_employee['gender'] = "m"
		else:
			new_employee['gender'] = "f"
		#get employee skills
		try:
			employee_skills = EmployeeSkill.objects.filter(userinfo = employee).order_by('value')
		except:
			 return HttpResponseServerError()
		#add employee skills to employee
		return_employee_skills = []
		for skill in employee_skills:
			return_employee_skills.append(skill.value)
		#add employee skills
		new_employee['employeeSkills'] = return_employee_skills
		#get employee education
		try:
			employee_education = EmployeeEducation.objects.filter(userinfo = employee).order_by('value')
		except:
			return HttpResponseServerError()
		return_employee_education = []
		for employee_education in employee_education:
			return_employee_education.append(employee_education.value)
		#add employee educaton to employee
		new_employee['employeeEducation'] = return_employee_education
		#get employee work experience
		try:
			employee_work_experience = EmployeeWorkExperience.objects.filter(userinfo = employee).order_by('title')
		except:
			return HttpResponseServerError()
		return_work_experience	= []
		#iterate through new work experience and make sure its addded
		for work_experience in employee_work_experience:
			new_work_experience={}
			new_work_experience['companyName'] = work_experience.company_name
			new_work_experience['title'] = work_experience.title
			new_work_experience['description'] = work_experience.description
			#append work experience to new work experience
			return_work_experience.append(new_work_experience)
		#add to return data
		new_employee['employeeWorkExperience'] = return_work_experience
		#add new employee to set of emp
		return_employees.append(new_employee)
		#return the employees
	return HttpResponse(json.dumps(return_employees))


#FUNCTION used to deal with employer selection.
#selection either "right" or "left"
@csrf_exempt
def employer_selection(request):
    #get post data
    try:
        post_data = json.loads(request.body)
        user_id = post_data.get('socialUserID')
        access_token= post_data.get('accessToken')
        job_id = post_data.get('jobID')
        employee_id = post_data.get('employeeID')
        selection = post_data.get('selection')

    except:
        return HttpResponseBadRequest()
    user = get_user(user_id, access_token)
    if user is None:
        return HttpResponseForbidden()
    #get employee that is swiped
    employee = UserInfo.objects.get(id = employee_id)
    try:
        employee = UserInfo.objects.get(id = employee_id)
    except Exception, e:
	print str(e)
        return HttpResponseServerError()

    #get job that we are using
    try:
        job = Job.objects.get(id = job_id)
    except Exception,e :
	print str(e)
        return HttpResponseServerError()
    #check to see what the selection was
    if selection == "right":
        is_chosen = True
    else:
        is_chosen = False
    #create new swipe
    try:
        employer_choice = EmployerChoice(employee = employee,
					employer = user,
					job = job,
					is_chosen =is_chosen)
        employer_choice.save()
        # if user swiped left then return HttpResponse
        if is_chosen ==False:
            return HttpResponse("no match")
    except:
        return HttpResponseServerError()
    #check to see is the employee has also already swiped right
    try:
	employee_choice = EmployeeChoice.objects.get(employee=employee, job= job, is_chosen = True)
	#mke employee choice viewed alreadt
	employee_choice.is_viewed = True
	employee_choice.save()
	job_match = JobMatch(employer = user,
			     employee = employee,
			     job = job,
			    )
	job_match.save()
	#also need to give employee new match message
	employee.has_new_match_employee = True
	employee.has_new_message_employee = True
	employee.save()
       #emplooyee has also swiped right, so we must return match
	return_data = {}
	return_data['jobMatchID'] = job_match.id
	return_employee = {}
	return_employee['id'] = employee.id
	return_employee['firstName'] = employee.first_name
	return_employee['lastName'] = employee.last_name
	return_employee['profilePictureURL'] = employee.profile_picture_url
	return_employee['socialUserID'] = employee.social_user_id
	return_employee['quickbloxID'] = employee.quickblox_id
	#return employer data
	return_employer={}
	return_employer['id'] = user.id
	return_employer['firstName'] = user.first_name
	return_employer['lastName'] = user.last_name
	return_employer['profilePictureURL'] = user.profile_picture_url
	return_employer['socialUserID'] = user.social_user_id
	return_employer['quickbloxID'] = user.quickblox_id
	#add job data
	return_job = {}
	return_job['title'] = job.title
	return_job['id'] = job.id
	#add data to return
	return_data['employee'] = return_employee
	return_data['employer'] = return_employer
	return_data['job'] = return_job
	print return_data
	return HttpResponse(json.dumps(return_data))
    except Exception, e:
	print e
	return HttpResponse('no match')




#FUNCTION USED TO GET JOBS IN AREA
def get_jobs_in_area(request):
    #get params
    try:
        user_id = request.GET.get('socialUserID')
        accessToken = request.GET.get('accessToken')
        current_latitude = request.GET.get('currentLatitude')
        current_longitude = request.GET.get('currentLongitude')
    except:
        raise HttpResponseBadRequest()
    #get user
    user = get_user(user_id, accessToken)
    if user == None:
        return HttpResponseForbidden()
    #get employee info
    try:
        employee_info = EmployeeInfo.objects.get(userinfo= user)
    except Exception, e :
        return HttpResponseServerError(e)
#check sessison cache to see if it exists, if now we add it.
#this is used so that we know what cards have been offered already

    #get salary requirements
    salary_low = employee_info.salary_low
    salary_high  = employee_info.salary_high
    #get distance
    distance = employee_info.distance_filter
    print current_latitude
    latitude = Decimal(current_latitude)
    longitude = Decimal(current_longitude)
    #round latitude and longitude
    latitude = round(latitude,6)
    longitude = round(longitude,6)
    latitude_lower_limit= latitude - 2
    latitude_upper_limit = latitude + 2
    longitude_lower_limit = longitude - 2
    longitude_upper_limit = longitude + 2
    # we need to get a random int so we can fetch 100 random records
    num_jobs = Job.objects.count() - 1
    if num_jobs == -1:
    	return HttpResponse()
    random_num = randint(0,num_jobs)
    print random_num
    #if there are gonna be less then 150 jobs
    if random_num - 150 < 0:
	random_num = 0
    if random_num + 150 > num_jobs:
	last_index = num_jobs
    else:
	last_index = random_num+150
    #try to get 100 jobs within user salary filters(guarantee and non guarantee)
    try:
	guarantee_jobs = Job.objects.filter(wage__gte= salary_low,
                            latitude__gte = latitude_lower_limit,
                            latitude__lte = latitude_upper_limit,
                            longitude__lte = longitude_upper_limit,
                            longitude__gte = longitude_lower_limit,
			    is_guarantee_pay = True,
			    is_filled = False,
			    is_completed  = False,
			    is_scraped=False,
			    ).exclude(posted_by = user).order_by('?')[:30]
    except Exception, e:
	print e
	return HttpResponseServerError()
    try:
	non_guarantee_jobs = Job.objects.filter(wage__gte= salary_low,
                            latitude__gte = latitude_lower_limit,
                            latitude__lte = latitude_upper_limit,
                            longitude__lte = longitude_upper_limit,
                            longitude__gte = longitude_lower_limit,
			    is_guarantee_pay = False ,
			    is_filled = False,
			    is_completed = False,
			    is_scraped = False,
			    ).exclude(posted_by = user).order_by('?')[:30]
    except:
	print "get jobs failed"
	return HttpResponseServerError()
    #if there are no jobs try getting email jobs return nothing
    print len(non_guarantee_jobs) + len(guarantee_jobs)
    if len(non_guarantee_jobs) == 0 and len(guarantee_jobs) == 0:
	try:
	    email_jobs = Job.objects.filter(
                            latitude__gte = latitude_lower_limit,
                            latitude__lte = latitude_upper_limit,
                            longitude__lte = longitude_upper_limit,
                            longitude__gte = longitude_lower_limit,
			    is_guarantee_pay = False ,
			    is_scraped =True,
			    ).order_by('?')[:30]
	except:
	    print "get jobs failed"
	    return HttpResponseServerError()
    else:
	email_jobs= []

    #get all jobs that have been selected so it doesnt repeat
    try:
	jobs_selected  =  EmployeeChoice.objects.filter(employee = user)
    except:
	print "job selection failed"
	return HttpResponseServerError()
    #change jobs to list add guarantee first
    print "EMAIL JOBS"
    print email_jobs
    jobs = []
    for job in guarantee_jobs:
	jobs.append(job)
    for job in non_guarantee_jobs:
	jobs.append(job)
    for job in email_jobs:
	jobs.append(job)

    #check to see if job has already been selected, if it has, remove from list
    for selected_job in jobs_selected:
	if selected_job.job in jobs:
	    jobs.remove(job)
    #check to see if jobs belong in the job class that user is looking for
    employee_job_types = EmployeeJobType.objects.filter(userinfo = user )
    # create list of job_types
    job_types=[]
    for employee_job_type in employee_job_types:
	job_types.append(employee_job_type.job_type.id)
    #remove jobs in job type and distance too far
    results=[]
    for job in jobs:
	if job.job_type.id in job_types:
	    print job
	    results.append(job)
    jobs=results
    results=[]
    for job in jobs:
	distance_from_user = get_distance(float(job.latitude), float(job.longitude),float(latitude),float(longitude))
	distance_from_user = float(distance_from_user)
	distance = float(distance)
	print distance
	if distance_from_user<distance:
	    results.append(job)
    jobs= results

    #prepare to return jobs
    return_jobs = []
    for job in jobs:
	new_job = {}
	new_job['id'] = job.id
	new_job['title'] = job.title
	new_job['wage'] = str(job.wage)
	new_job['description'] = job.description
	new_job['type'] = job.job_type.name
	new_job['latitude'] = str(job.latitude)
	new_job['longitude'] = str(job.longitude)
	new_job['job_image_url'] = job.job_image_url
	new_job['posted_by'] ={}
	new_job['posted_by']['name'] = job.posted_by.first_name + job.posted_by.last_name
	new_job['posted_by']['id'] = job.posted_by.id
	new_job['isScraped'] = job.is_scraped
	if job.is_guarantee_pay == True:
	    new_job['isGuarantee'] = True
	else:
	    new_job['isGuarantee'] = False
	if job.is_scraped:
	   new_job['email'] = job.email
	   new_job['scrapedFrom'] = job.scraped_from
	   new_job['job_url'] = job.job_url
	return_jobs.append(new_job)
    #if there are no jobs
    if len(return_jobs) == 0:
	return HttpResponse()
    print return_jobs
    #shufffle the jobs so all users dont keep getting the same oe
    #random.shuffle(return_jobs)
    #check to see if cards have been assigned already
    return HttpResponse(json.dumps(return_jobs))


@require_POST
@csrf_exempt
#FUNCTION used to handle employee selection
def employee_selection(request):
    try:
	post_data = json.loads(request.body)
	user_id = post_data.get('socialUserID')
	access_token = post_data.get('accessToken')
	job_id = post_data.get('jobID')
	selection = post_data.get('selection')
    except:
	return HttpResponseBadRequest()
    user = get_user(user_id, access_token)
    if user == None:
	return HttpResponseForbidden()
    #get job
    try:
	job = Job.objects.get(id = job_id)
    except:
	print "job get error"
	raise HttpResponseServerError()
    if selection == "right":
	choice = True
    elif selection == "left":
	choice = False
    #create new selection
    selection = EmployeeChoice(employee = user,
				job = job,
				is_chosen = choice,
				)
    selection.save()
    # make if its a scraped job then send an email
    if job.is_scraped:
	try:
	    mandrill_client = mandrill.Mandrill('GJ4jKGoh5IE7IUGW8zsA-g')
	    message = {'from_email':'info@getjobswiper.com',
	    			'to':[{'email':job.email,'type':'to'}],
		    'html':"<p>"+user.first_name+ " "+ user.last_name+" has applied for your job "+ job.title+"</p>"+
		    "<p> Contact them at"+ user.email+ "</p>"+"<p>Email sent from <a href = 'http://www.getjobswiper.com'>Jobswiper</a></p>"}
	    result = mandrill_client.messages.send(message = message, async = False)
	except mandrill.Error, e:
	    print 'A mandrill error occurred: %s - %s' % (e.__class__, e)
	    return HttpResponseServerError()
	#send email
    #check to see if employer has swiped right to employee for that particular job
    try:
	employer_choice = EmployeeChoice.objects.get(job = job, employee = user)
	#there is a match, so create a match and return a match
	employer = employer_choice.employer
	match = JobMatch(employer = employer_choice.employer,
			employee = user,
			job = job,
			)
	match.save()
	#create match messages

	return_data = {}
        return_data['jobMatchID'] = job_match.id
        return_employee = {}
        return_employee['id'] = user.id
        return_employee['firstName'] = user.first_name
        return_employee['lastName'] = user.last_name
        return_employee['profilePictureURL'] = user.profile_picture_url
        return_employee['socialUserID'] = user.social_user_id
        return_employee['quickbloxID'] = user.quickblox_id
        #return employer data
        return_employer={}
        return_employer['id'] = employer.id
        return_employer['firstName'] = employer.first_name
        return_employer['lastName'] = employer.last_name
        return_employer['profilePictureURL'] = employer.profile_picture_url
        return_employer['socialUserID'] = employer.social_user_id
        return_employer['quickbloxID'] = employer.quickblox_id
        #add job data
        return_job = {}
        return_job['title'] = job.title
        return_job['id'] = job.id
        #add data to return
        return_data['employee'] = return_employee
        return_data['employer'] = return_employer
        return_data['job'] = return_job
        return HttpResponse(json.dumps(return_data))
    except:
	#there is no match present
	return HttpResponse('no match')
#GET job details
@require_GET
@csrf_exempt
def get_job_details(request):
    try:
	job_id  = request.GET.get('id')
    except:
	return HttpResponseBadRequest()

    try:
	job = Job.objects.get(id = job_id)
    except:
	return HttpResponseServerError()

    return_job = {}
    return_job['title'] =job.title
    return_job['wage'] = str(job.wage)
    return_job['description'] = job.description
    return_job['jobType'] = job.job_type.name
    return_job['jobImageUrl'] = job.job_image_url
    return_job['postedBy'] ={}
    return_job['postedBy']['firstName'] = job.posted_by.first_name
    return_job['postedBy']['id'] = job.posted_by.id
    return_job['postedBy']['profilePictureURL'] = job.posted_by.profile_picture_url
    return HttpResponse(json.dumps(return_job))



#EDIT profile information
@csrf_exempt
def edit_job(request):
    if request.method == "GET":
	try:
	    job_id = request.GET.get('jobID')
	    user_id = request.GET.get('userID')
	    access_token = request.GET.get('accessToken')
	except Exception,e :
	    print str(e)
	    return HttpResponseBadRequest()

	user  = get_user(user_id, access_token)
	if user == None:
	    return HttpResponseForbidden()
	print job_id
	print user
	try:
	    job = Job.objects.get(id = job_id, posted_by = user)
	except:
	    return HttpResponseServerError()
	return_job= {}
	return_job['id'] = job.id
	return_job['title'] = job.title
	return_job['wage'] = str(job.wage)
	return_job['jobClass'] = job.job_type.id
	return_job['description'] = job.description
	return HttpResponse(json.dumps(return_job))

    elif request.method == "POST":
	try:
	    post_data = json.loads(request.body)
	    user_id = post_data.get('userID')
	    access_token = post_data.get('accessToken')
	    job = post_data.get('job')
	except:
	    return HttpResponseBadRequest()
	print job
	user = get_user(user_id, access_token)
	if user == None:
	    return HttpResponseForbidden()
	#get job
	try:
	    old_job = Job.objects.get(id = job.get('id'),posted_by = user)
	except:
	    print "job get error"
	    raise HttpResponseForbidden()

	old_job.title = job.get('title')
	old_job.wage = job.get('wage')
	try:
	    job_type= JobType.objects.get(id= job.get('jobClass'))
	except Exception,e :
	    print e
	    return HttpResponseServerError()
	old_job.job_type = job_type
	old_job.description = job.get('description')
	old_job.save()
	return HttpResponse()

@require_POST
@csrf_exempt
#FUNCTION DELETE JOB
def delete_job(request):
    try:
	post_data = json.loads(request.body)
	user_id = post_data.get('userID')
	access_token = post_data.get('accessToken')
	job_id = post_data.get('jobID')
    except:
	return HttpResponseBadRequest()
    user = get_user(user_id, access_token)
    print user
    if user == None:
	return HttpResponseForbidden()
    #get job
    print user_id
    print user
    try:
	job = Job.objects.get(id = job_id,posted_by = user)
    except:
	return HttpResponseForbidden()
    #delete matches and choices
    try:
	JobMatch.objects.filter(job = job).delete()
	EmployeeChoice.objects.filter(job=job).delete()
	EmployerChoice.objects.filter(job=job).delete()
    except Exception,e:
	print e
	return HttpResponseServerError()
    #delete job
    job.delete()
    return HttpResponse()
#offer match the job
@csrf_exempt
@require_POST
def offer_match(request):
    try:
	post_data = json.loads(request.body)
	user_id = post_data.get('userID')
	access_token = post_data.get('accessToken')
	match_id= post_data.get('matchID')
    except:
	return HttpResponseBadRequest()
    user = get_user(user_id, access_token)
    if user == None:
	return HttpResponseForbidden()
    try:
	job_match = JobMatch.objects.get(id = match_id,employer = user)
	job_match.is_offered = True
	job_match.last_updated = datetime.datetime.now()
	job_match.save()
    except Exception,e :
	print e
	return HttpResponseForbidden()
    #delete matches and choices
    return HttpResponse()

#remove match offer
@csrf_exempt
@require_POST
def remove_match(request):
    try:
	post_data = json.loads(request.body)
	user_id = post_data.get('userID')
	access_token = post_data.get('accessToken')
	match_id= post_data.get('matchID')
	user_type = post_data.get('userType')
    except:
	return HttpResponseBadRequest()
    user = get_user(user_id, access_token)
    if user == None:
	return HttpResponseForbidden()
    try:
	if user_type =="employee":
	    job_match = JobMatch.objects.get(id = match_id,employee = user)
	elif user_type == "employer":
	    job_match = JobMatch.objects.get(id = match_id, employer = user)
	job_match.is_unmatched = True
	job_match.save()
    except:
	return HttpResponseForbidden()
    #delete matches and choices
    return HttpResponse()

#accept match offer
@csrf_exempt
@require_POST
def accept_offer(request):
    try:
	post_data = json.loads(request.body)
	user_id = post_data.get('userID')
	access_token = post_data.get('accessToken')
	match_id= post_data.get('matchID')
    except:
	return HttpResponseBadRequest()
    user = get_user(user_id, access_token)
    if user == None:
	return HttpResponseForbidden()
    try:
	job_match = JobMatch.objects.get(id = match_id,employee = user)
	job_match.is_hired = True
	job_match.last_updated = datetime.datetime.now()
	job_match.save()
	#fill job
	job = job_match.job
	job.is_filled = True
	job.save()

    except:
	return HttpResponseForbidden()
    #delete matches and choices
    return HttpResponse()
#complete job
@csrf_exempt
@require_POST
def complete_job(request):
    try:
	post_data = json.loads(request.body)
	user_id = post_data.get('userID')
	access_token = post_data.get('accessToken')
	match_id= post_data.get('matchID')
    except:
	return HttpResponseBadRequest()
    user = get_user(user_id, access_token)
    if user == None:
	return HttpResponseForbidden()
    try:
	job_match = JobMatch.objects.get(id = match_id,employer = user)
	job_match.last_updated = datetime.datetime.now()
	job = job_match.job
	#TODO
	#if job is guarantee pay, credit users account
	if job.is_guarantee_pay == True:
	    employee_info = EmployeeInfo.objects.get(userinfo = job_match.employee)
	    amount_made = employee_info.amount_made + job.wage
	    employee_info.amount_made = amount_made
	    employee_info.save()
	job.is_completed=True
	job.save()
    except Exception,e:
	print e
	return HttpResponseForbidden()
    #delete matches and choices
    return HttpResponse()

#get matches for all jobs
@csrf_exempt
@require_GET
def get_matches(request):
    try:
	user_id = request.GET.get('userID')
	access_token = request.GET.get('accessToken')
	user_type = request.GET.get('userType')
    except:
	return HttpResponseBadRequest()
    print user_type

    user = get_user(user_id, access_token)
    if user == None:
	return HttpResponseForbidden()
    #if usertype is employee, we need to get employee match, eslse get employer match
    if user_type == "employee":
	matches = JobMatch.objects.filter(employee= user, is_unmatched = False).order_by('-last_updated')
    elif user_type == "employer":
	matches = JobMatch.objects.filter(employer = user, is_unmatched = False).order_by('-last_updated')

    #iteate through all matches and organize based on hire
    user_matches = []
    for match in matches:
	#get match job
	match_job = match.job
	#get other user match
	new_match = {}
	if user_type == "employer":
	    other_user = match.employee
	    new_match['isEmployer'] = True
	elif user_type == "employee":
	    other_user = match.employer
	    new_match['isEmployer'] = False
	new_match['id'] = match.id
	if match_job.is_guarantee_pay:
	    new_match['isGuaranteePay'] = True
	else:
	    new_match['isGuaranteePay'] = False
	new_match['job'] = {}
	new_match['job']['title'] = match_job.title
	new_match['job']['wage'] = str(match_job.wage)
	new_match['job']['id'] = match_job.id
	new_match['otherUser'] = {}
	new_match['otherUser']['id'] = other_user.id
	new_match['otherUser']['name'] =  other_user.first_name
	new_match['otherUser']['profilePictureURL'] = other_user.profile_picture_url
	#in order for a job to be offered, it cant be also hired
	if match.is_offered and match.is_hired == False:
	    new_match['isOffered'] = True
	else:
	    new_match['isOffered'] = False
	new_match['isHired'] = match.is_hired
	new_match['isCompleted'] = match_job.is_completed
	#if jpob is compelted, then job can't be hired
	if match_job.is_completed:
	    new_match['isHired'] = False
	#cehck to see if match is just matched
	if match.is_hired == False and match_job.is_completed == False and match.is_offered == False:
	    new_match['isMatched'] = True
	else:
	    new_match['isMatched'] = False
	new_match['dateAdded'] = match.date_added.isoformat()
	user_matches.append(new_match)
    return HttpResponse(json.dumps(user_matches))
@csrf_exempt
def add_scraped_jobs_spreadsheet(request):
	if request.user.is_superuser:
		if request.method == "GET":
			return render_to_response("add-job-spreadsheet.html", context_instance=RequestContext(request))

		elif request.method == "POST":
			error_messages=[]
			form = request.FILES['csvform']
			file_name = form.name
			extension = file_name.split('.')[1]
			if extension != "csv":
				form_args={}
				form_args['error_messages'] = ["file must be of type '.csv'"]
				return render_to_response("add-job-spreadsheet.html", form_args,context_instance=RequestContext(request))
			path = default_storage.save('tmp/' + str(form), ContentFile(form.read()))
			tmp_file_path = os.path.join(settings.MEDIA_ROOT, path)
			with open(tmp_file_path, 'rU') as csvfile:
				print csvfile
				spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
				job_types = JobType.objects.filter()
				all_jobs=[]
				#skip first line
				firstline = True
				for row in spamreader:
					if firstline:    #skip first line
						firstline = False
						continue
					#split all values in row
					print row
					row= str(row[0])
					new_job_values = row.split(',')
					print new_job_values
					title= new_job_values[0]
					email = new_job_values[1]
					try:
						job_type = JobType.objects.get(name = new_job_values[2])
					except:
						error_messages.append("%s couldn't be added because of wrong job type" % new_job_values[2])
						continue
					print new_job_values
					try:
						wage = Decimal(new_job_values[3])
					except:
						error_messages.append("%s couldn't be added because it is not a wage" % new_job_values[2])
						continue
					description = new_job_values[4][:1000]
					gps = get_gps(new_job_values[5],new_job_values[6])
					print gps
					try:
						latitude = Decimal(gps[0])
						longitude = Decimal(gps[1])
					except:
						error_messages.append("%s couldn't be added because city is not right" % new_job_values[2])
						continue

					userinfo = UserInfo.objects.filter()[0]
					job= Job(posted_by = userinfo,
						title = title,
						wage = wage,
						description = description,
						email =email,
						job_type = job_type,
						is_scraped = True,
						latitude = latitude,
						longitude = longitude
						)
					all_jobs.append(job)
				print all_jobs

			#bulk create jobs
			form_args={}
			form_args['error_messages':error_messages]
			return render_to_response("add-job-spreadsheet.html", form_args,context_instance=RequestContext(request))

	else:
		return HttpResponse("must be admin. goto /admin and login")

@csrf_exempt
def add_scraped_jobs(request):
	if request.user.is_staff:
		if request.method == "GET":
			jobtypes= JobType.objects.all()
			jobcities = job_cities()
			scrapersites = scraper_sites()
			form_args={'jobtypes':jobtypes,'jobcities':jobcities,'scrapersites':scrapersites}
			return render_to_response("add-job.html",form_args, context_instance=RequestContext(request))
		elif request.method == "POST":
			jobtypes= JobType.objects.all()
			jobcities = job_cities()
			scrapersites = scraper_sites()
			form_args={'jobtypes':jobtypes,'jobcities':jobcities,'scrapersites':scrapersites}
			title = request.POST.get('title')
			email = request.POST.get('email')
			description = request.POST.get('description')
			job_type = request.POST.get('job_type')
			job_url= request.POST.get('url')
			scraped_from = request.POST.get('scraper_site')
			city = request.POST.get('city')
			wage = request.POST.get('wage')
			if title == "":
				form_args['errormessage']= 'job needs title'
				return render_to_response("add-job.html",form_args, context_instance=RequestContext(request))

			if email == "":
				form_args['errormessage']= 'email needs value'
				return render_to_response("add-job.html",form_args, context_instance=RequestContext(request))


			if job_url == "":
				form_args['errormessage']= 'url needs value'
				return render_to_response("add-job.html",form_args, context_instance=RequestContext(request))
			if wage == "":
				wage = 0.0

			job_type = request.POST.get('job_type')
			description = description[:1000]
			userinfo = UserInfo.objects.filter()[0]
			job_type = JobType.objects.get(id = job_type)
			gps = get_gps(city)
			latitude = gps[0]
			longitude = gps[1]
			job = Job(posted_by = userinfo,
						title = title,
						wage = wage,
						description = description,
						email =email,
						job_type = job_type,
						is_scraped = True,
						latitude = latitude,
						longitude = longitude,
						job_url=job_url,
						scraped_from= scraped_from
						)
			job.save()
			form_args['city'] = city
			form_args['scraped_from'] = scraped_from
			form_args['job_title'] = title
			return render_to_response("add-job.html",form_args, context_instance=RequestContext(request))


	else:
		return HttpResponseForbidden("Must be admin. Go to /admin to login")
def get_distance(lat1, long1, lat2, long2):

    # Convert latitude and longitude to
    # spherical coordinates in radians.
    degrees_to_radians = math.pi/180.0

    # phi = 90 - latitude
    phi1 = (90.0 - lat1)*degrees_to_radians
    phi2 = (90.0 - lat2)*degrees_to_radians

    # theta = longitude
    theta1 = long1*degrees_to_radians
    theta2 = long2*degrees_to_radians

    # Compute spherical distance from spherical coordinates.

    # For two locations in spherical coordinates
    # (1, theta, phi) and (1, theta, phi)
    # cosine( arc length ) =
    #    sin phi sin phi' cos(theta-theta') + cos phi cos phi'
    # distance = rho * arc length

    cos = (math.sin(phi1)*math.sin(phi2)*math.cos(theta1 - theta2) +
           math.cos(phi1)*math.cos(phi2))
    arc = math.acos( cos )

    # Remember to multiply arc by the radius of the earth
    # in your favorite set of units to get length.
    arc = arc *6373
    return arc
def get_gps(city):
	gps_stuff={
	"Alturas,CA" :[41.48 ,-120.53]  ,
	"Arcata,CA" :[40.98 ,-124.10]  ,
	"Bakersfield,CA" :[35.43 ,-119.05]  ,
	"Beale AFB,CA" :[39.13 ,-121.45]  ,
	"Beaumont,CA" :[33.93 ,-116.95]  ,
	"Bicycle Lk,CA" :[35.28 ,-116.62]  ,
	"Big Bear Apt,CA" :[34.27 ,-116.68]  ,
	"Bishop,CA" :[37.60 ,-118.60]  ,
	"Blue Canyon,CA" :[39.28 ,-120.70]  ,
	"Blythe,CA" :[33.62 ,-114.72]  ,
	"Burbank,CA" :[34.20 ,-118.37]  ,
	"Camp Pendlet,CA" :[33.30 ,-117.35]  ,
	"Campo,CA" :[32.62 ,-116.47]  ,
	"Carlsbad,CA" :[33.13 ,-117.28]  ,
	"Castle AFB,CA" :[37.38 ,-120.57]  ,
	"Chico,CA" :[39.78 ,-121.85]  ,
	"China Lake,CA" :[35.68 ,-117.68]  ,
	"Chino,CA" :[33.97 ,-117.63]  ,
	"Concord,CA" :[37.98 ,-122.05]  ,
	"Crescent Cty,CA" :[41.78 ,-124.23]  ,
	"Daggett,CA" :[34.87 ,-116.78]  ,
	"Edwards AFB,CA" :[34.90 ,-117.88]  ,
	"El Centro,CA" :[32.82 ,-115.68]  ,
	"El Monte,CA" :[34.08 ,-118.03]  ,
	"El Toro,CA" :[33.67 ,-117.73]  ,
	"Eureka,CA" :[41.33 ,-124.28]  ,
	"Fort Hunter,CA" :[36.00 ,-121.32]  ,
	"Fort Ord,CA" :[36.68 ,-121.77]  ,
	"Fresno,CA" :[36.77 ,-119.72]  ,
	"Fullerton,CA" :[33.87 ,-117.97]  ,
	"George AFB,CA" :[34.58 ,-117.38]  ,
	"Hawthorne,CA" :[33.92 ,-118.33]  ,
	"Hayward,CA" :[37.65 ,-122.12]  ,
	"Imperial,CA" :[32.83 ,-115.57]  ,
	"Imperial Bch,CA" :[32.57 ,-117.12]  ,
	"La Verne,CA" :[34.10 ,-117.78]  ,
	"Lake Tahoe,CA" :[38.90 ,-120.00]  ,
	"Lancaster,CA" :[34.73 ,-118.22]  ,
	"Lemoore NAS,CA" :[36.33 ,-119.95]  ,
	"Livermore,CA" :[37.70 ,-121.82]  ,
	"Long Beach,CA" :[33.82 ,-118.15]  ,
	"Los Alamitos,CA" :[33.78 ,-118.05]  ,
	"Los Angeles,CA" :[33.93 ,-118.40]  ,
	"Mammoth Lks,CA" :[37.63 ,-118.92]  ,
	"March AFB,CA" :[33.88 ,-117.27]  ,
	"Marysville,CA" :[39.10 ,-121.57]  ,
	"Mather AFB,CA" :[38.57 ,-121.30]  ,
	"Mcclellan,CA" :[38.67 ,-121.40]  ,
	"Merced,CA" :[37.28 ,-120.52]  ,
	"Miramar NAS,CA" :[32.87 ,-117.15]  ,
	"Modesto,CA" :[37.63 ,-120.95]  ,
	"Moffet NAS,CA" :[37.42 ,-122.05]  ,
	"Mojave,CA" :[35.05 ,-118.15]  ,
	"Montague,CA" :[41.73 ,-122.53]  ,
	"Monterey,CA" :[36.58 ,-121.85]  ,
	"Mount Shasta,CA" :[41.32 ,-122.32]  ,
	"Mount Wilson,CA" :[34.23 ,-118.07]  ,
	"Napa,CA" :[38.22 ,-122.28]  ,
	"Needles,CA" :[34.77 ,-114.62]  ,
	"North Is,CA" :[32.70 ,-117.20]  ,
	"Norton AFB,CA" :[34.10 ,-117.23]  ,
	"Oakland,CA" :[37.73 ,-122.22]  ,
	"Ontario Intl,CA" :[34.05 ,-117.62]  ,
	"Oxnard,CA" :[34.20 ,-119.20]  ,
	"Palm Springs,CA" :[33.83 ,-116.50]  ,
	"Palmdale,CA" :[35.05 ,-118.13]  ,
	"Palo Alto,CA" :[37.47 ,-122.12]  ,
	"Paso Robles,CA" :[35.67 ,-120.63]  ,
	"Pillaro Pt,CA" :[37.83 ,-122.83]  ,
	"Point Mugu,CA" :[34.12 ,-119.12]  ,
	"Pt Arena,CA" :[39.58 ,-124.22]  ,
	"Pt Arguello,CA" :[34.95 ,-121.12]  ,
	"Pt Piedras,CA" :[35.67 ,-121.28]  ,
	"Pt Piedras,CA" :[36.12 ,-121.47]  ,
	"Red Bluff,CA" :[40.15 ,-122.25]  ,
	"Redding,CA" :[40.50 ,-122.30]  ,
	"Riverside,CA" :[33.95 ,-117.45]  ,
	"Sacramento,CA" :[38.52 ,-121.50]  ,
	"Sacramento,CA" :[38.70 ,-121.60]  ,
	"Salinas,CA" :[36.67 ,-121.60]  ,
	"San Carlos,CA" :[37.52 ,-122.25]  ,
	"San Clemente,CA" :[33.42 ,-117.62]  ,
	"San Clemente,CA" :[33.02 ,-118.58]  ,
	"San Diego,CA" :[32.82 ,-117.13]  ,
	"San Diego,CA" :[32.73 ,-117.17]  ,
	"San Diego,CA" :[32.57 ,-116.98]  ,
	"San Diego,CA" :[32.57 ,-116.98]  ,
	"San Diego,CA" :[32.82 ,-116.97]  ,
	"San Francisco,CA" :[37.75 ,-122.68]  ,
	"San Francisco,CA" :[37.62 ,-122.38]  ,
	"San Jose,CA" :[37.37 ,-121.92]  ,
	"San Jose/Rei,CA" :[37.33 ,-121.82]  ,
	"San Luis Obi,CA" :[35.23 ,-120.65]  ,
	"San Mateo,CA" :[33.38 ,-117.58]  ,
	"San Miguel,CA" :[34.03 ,-120.40]  ,
	"San Nic Isl,CA" :[33.25 ,-119.45]  ,
	"Sandburg,CA" :[34.75 ,-118.73]  ,
	"Santa Ana,CA" :[33.67 ,-117.88]  ,
	"Santa Barb,CA" :[34.43 ,-119.83]  ,
	"Santa Maria,CA" :[34.90 ,-120.45]  ,
	"Santa Monica,CA" :[34.02 ,-118.45]  ,
	"Santa Rosa,CA" :[38.52 ,-122.82]  ,
	"Shelter Cove,CA" :[40.03 ,-124.07]  ,
	"Siskiyou,CA" :[41.78 ,-122.47]  ,
	"Stockton,CA" :[37.90 ,-121.25]  ,
	"Superior Val,CA" :[35.33 ,-117.10]  ,
	"Susanville,CA" :[40.63 ,-120.95]  ,
	"Thermal,CA" :[33.63 ,-116.17]  ,
	"Torrance,CA" :[33.80 ,-118.33]  ,
	"Travis AFB,CA" :[38.27 ,-121.93]  ,
	"Truckee-Tahoe,CA" :[39.32 ,-120.13]  ,
	"Tustin Mcas,CA" :[33.70 ,-117.83]  ,
	"Twenty9 Palm,CA" :[34.28 ,-116.15]  ,
	"Ukiah,CA" :[39.13 ,-123.20]  ,
	"Van Nuys,CA" :[34.22 ,-118.48]  ,
	"Vandenberg,CA" :[35.20 ,-120.95]  ,
	"Vandenberg,CA" :[35.20 ,-120.95]  ,
	"Visalia,CA" :[36.32 ,-119.40]  ,
	#BC
	"Richmond,BC": [49.166973, -123.137173],
	"Surrey,BC": [49.181075, -122.844049],
	"Burnaby,BC": [49.181075, -122.844049],
	"Coquitlam,BC": [49.272165, -122.797960],
	"Maple Ridge,BC": [49.219271, -122.600206],
	"Delta,BC": [49.097130, -123.014253],
	"Langley,BC": [49.071046, -122.551454],
	"Abbotsford,BC": [49.053950, -122.337221],
	"Mission,BC": [49.181075, -122.844049],
	"Vancouver,BC":[49.2827000,-123.1207000]
		}
	gps = gps_stuff.get(city)
	return gps

def job_cities():
	cities=[
	"Richmond,BC",
"Surrey,BC",
"Burnaby,BC",
"Coquitlam,BC",
"Maple Ridge,BC",
"Delta,BC",
"Langley,BC",
"Abbotsford,BC",
"Mission,BC",
" Aameda NAS,CA",
"Alturas,CA",
"Arcata,CA",
"Bakersfield,CA",
"Beale AFB,CA",
"Beaumont,CA",
"Bicycle Lk,CA",
"Big Bear Apt,CA",
"Bishop,CA",
"Blue Canyon,CA",
"Blythe,CA",
"Burbank,CA",
"Camp Pendlet,CA",
"Campo,CA",
"Carlsbad,CA",
"Castle AFB,CA",
"Chico,CA",
"China Lake,CA",
"Chino,CA",
"Concord,CA",
"Crescent Cty,CA",
"Daggett,CA",
"Edwards AFB,CA",
"El Centro,CA",
"El Monte,CA",
"El Toro,CA",
"Eureka,CA",
"Fort Hunter,CA",
"Fort Ord,CA",
"Fresno,CA",
"Fullerton,CA",
"George AFB,CA",
"Hawthorne,CA",
"Hayward,CA",
"Imperial,CA",
"Imperial Bch,CA",
"La Verne,CA",
"Lake Tahoe,CA",
"Lancaster,CA",
"Lemoore NAS,CA",
"Livermore,CA",
"Long Beach,CA",
"Los Alamitos,CA",
"Los Angeles,CA",
"Mammoth Lks,CA",
"March AFB,CA",
"Marysville,CA",
"Mather AFB,CA",
"Mcclellan,CA",
"Merced,CA",
"Miramar NAS,CA",
"Modesto,CA",
"Moffet NAS,CA",
"Mojave,CA",
"Montague,CA",
"Monterey,CA",
"Mount Shasta,CA",
"Mount Wilson,CA",
"Napa,CA",
"Needles,CA",
"North Is,CA",
"Norton AFB,CA",
"Oakland,CA",
"Ontario Intl,CA",
"Oxnard,CA",
"Palm Springs,CA",
"Palmdale,CA",
"Palo Alto,CA",
"Paso Robles,CA",
"Pillaro Pt,CA",
"Point Mugu,CA",
"Pt Arena,CA",
"Pt Arguello,CA",
"Pt Piedras,CA",
"Pt Piedras,CA",
"Red Bluff,CA",
"Redding,CA",
"Riverside,CA",
"Sacramento,CA",
"Sacramento,CA",
"Salinas,CA",
"San Carlos,CA",
"San Clemente,CA",
"San Clemente,CA",
"San Diego,CA",
"San Diego,CA",
"San Diego,CA",
"San Diego,CA",
"San Diego,CA",
"San Francisco,CA",
"San Francisco,CA",
"San Jose,CA",
"San Jose/Rei,CA",
"San Luis Obi,CA",
"San Mateo,CA",
"San Miguel,CA",
"San Nic Isl,CA",
"Sandburg,CA",
"Santa Ana,CA",
"Santa Barb,CA",
"Santa Maria,CA",
"Santa Monica,CA",
"Santa Rosa,CA",
"Shelter Cove,CA",
"Siskiyou,CA",
"Stockton,CA",
"Superior Val,CA",
"Susanville,CA",
"Thermal,CA",
"Torrance,CA",
"Travis AFB,CA",
"Truckee-Tahoe,CA",
"Tustin Mcas,CA",
"Twenty9 Palm,CA",
"Ukiah,CA",
"Van Nuys,CA",
"Vandenberg,CA",
"Vandenberg,CA",
"Visalia,CA"
]
	return cities

def scraper_sites():
	sites= ['craigslist','kijiji','monster']
	return sites
