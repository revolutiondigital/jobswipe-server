from django.db import models
from django import forms
from django.contrib.auth.models import User
from django.conf import settings
from django.db.models.fields import IntegerField
from jobs.models import JobType

# Create your models here.
class BigIntegerField(models.IntegerField):
    empty_strings_allowed=False
    def get_internal_type(self):
        return "BigIntegerField"
    def db_type(self, connection):
        return 'bigint' # Note this won't work with Oracle.

class UserInfo(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    profile_picture_url = models.CharField(max_length = 500)
    sex= models.CharField(max_length=20,blank = False)
    email = models.EmailField(max_length=254)
    #birth_date = models.DateField()
    address= models.CharField(max_length=200, blank = True, null = True)
    city = models.CharField(max_length=100, blank = True,null = True)
    region = models.CharField(max_length=100, blank = True, null = True)
    country = models.CharField(max_length=100,blank = True, null = True)
    postal_code = models.CharField(max_length = 20, blank = True, null = True)
    last_latitude = models.DecimalField(max_digits=11, decimal_places=7)
    last_longitude = models.DecimalField(max_digits=11, decimal_places=7)
    is_employee= models.BooleanField(default = False)
    is_employer =models.BooleanField(default= False)
    is_facebook= models.BooleanField()
    is_linkedin = models.BooleanField()
    social_user_id = BigIntegerField()
    #access token for facebook or linked in
    access_token = models.CharField(max_length=500)
    #quickblox id
    quickblox_id = models.BigIntegerField(blank=True, null = True)
    timezone = models.IntegerField()
    #new notifications
    has_new_match_employee= models.BooleanField(default = False)
    has_new_message_employee =models.BooleanField(default= False)
    has_new_match_employer= models.BooleanField(default = False)
    has_new_message_employer =models.BooleanField(default= False)


class EmployeeInfo(models.Model):
    userinfo = models.ForeignKey(UserInfo)
    salary_low = models.PositiveIntegerField()
    salary_high = models.PositiveIntegerField()
    description = models.TextField(null = True, blank = True)
    distance_filter = models.PositiveIntegerField(default=50)
    amount_made = models.DecimalField(max_digits=8,decimal_places=2,default=0)

class EmployerInfo(models.Model):
    userinfo = models.ForeignKey(UserInfo)
    name = models.CharField(max_length = 100)
    description = models.TextField(null = True, blank = True )
    num_jobs_posted = models.PositiveIntegerField(default = 0)

class EmployeeSkill(models.Model):
    userinfo = models.ForeignKey(UserInfo)
    value = models.CharField(max_length = 100)

class EmployeeEducation(models.Model):
    userinfo = models.ForeignKey(UserInfo)
    value = models.CharField(max_length = 100)
    institution = models.CharField(max_length=200)

class EmployeeWorkExperience(models.Model):
    userinfo = models.ForeignKey(UserInfo)
    company_name = models.CharField(max_length = 100)
    title = models.CharField(max_length = 100)
    description = models.CharField(max_length = 200)


#jobtypes that each employee is looking for
class EmployeeJobType(models.Model):
	userinfo = models.ForeignKey(UserInfo)
	job_type = models.ForeignKey(JobType)


class EmployeeChequeRequest(models.Model):
    employee = models.ForeignKey(UserInfo)
    date_requested = models.DateTimeField(auto_now_add = True)
    amount_made = models.DecimalField(max_digits=8,decimal_places=2)
    is_sent = models.BooleanField(default = False)

