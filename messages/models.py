from django.db import models
# Create your models here.

class MatchMessage(models.Model):
    employee = models.ForeignKey('userinfo.UserInfo', related_name="matchmessage.employee")
    employer = models.ForeignKey('userinfo.UserInfo', related_name="matchmessage.employer")
    match = models.ForeignKey('jobs.JobMatch')
    is_new_employer = models.BooleanField(default= True)
    is_new_employee = models.BooleanField(default = True)
    last_message_text = models.CharField(max_length= 200,default="")
    last_message_sent = models.DateTimeField(auto_now_add = True)
    chatroom_id = models.CharField(max_length=100,blank = True, null = True)



