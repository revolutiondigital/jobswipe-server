from decimal import *
import datetime
from django.conf import settings
from django.shortcuts import render_to_response, render
from django.template.defaulttags import csrf_token
from django.template import RequestContext
from django.contrib.auth.models import User,Group
from django.contrib.auth import *
from django.http import  *
from django.conf import settings
from django.middleware.csrf import get_token
import json
import ast
from userinfo.models import *
from jobs.models import *
from messages.models import *
from userinfo.views import get_user
from django.views.decorators.http import *
from django.contrib.auth.decorators import login_required
import urllib2
from django.views.decorators.csrf import *
from decimal import Decimal
from django.db.models import *
from random import randint

#FUNCTION used to get all the different job classes
@require_GET
def get_match_messages(request):
#fetch query param
    try:
	user_id = request.GET.get('socialUserID')
	access_token= request.GET.get('accessToken')
	user_type =  request.GET.get('userType')

    except:
	raise HttpResponseBadRequest()

    #get user
    print user_id
    print access_token
    print user_type
    user = get_user(user_id, access_token)
    if user == None:
	return HttpResponseForbidden()

    #get all the matches
    try:
	#if user type is employee then grab employees
	if user_type == "employee":
	    match_messages = JobMatch.objects.filter(employee = user).order_by('-last_updated')
	elif user_type == "employer":

	    match_messages = JobMatch.objects.filter(employer = user).order_by('-last_updated')
	#else grab employer data
    except:
	return HttpResponseServerError()
    print match_messages

    return_match_messages = []
    for match_message in match_messages:
    #we need to make sure that no users are displayed twice
	if len(return_match_messages)>0:
	    is_inside = False
	    if user_type == "employer":
		for item in return_match_messages:
		    if match_message.employee.id ==  item.get('user').get('id'):
			is_inside = True
			break
	    elif user_type == "employee":
		for item in return_match_messages:
		    if match_message.employer.id == item.get('user').get('id'):
			is_inside = True
			break
	    #if user in inside list
	    if is_inside == True:
		continue

	new_match_message ={}
	new_match_message['id'] = match_message.id
	#if user is employer we need to return different  employee data, not employers that are matched
	if user_type == "employer":
	    new_match_message['user'] ={}
	    new_match_message['user']['firstName'] =  match_message.employee.first_name
	    new_match_message['user']['lastName'] = match_message.employee.last_name
	    new_match_message['user']['socialUserID'] = match_message.employee.social_user_id
	    new_match_message['user']['id'] = match_message.employee.id
	    new_match_message['user']['profilePictureURL'] = match_message.employee.profile_picture_url
	    new_match_message['isUnread'] = match_message.is_new_employer
	    new_match_message['user']['quickbloxID'] = match_message.employee.quickblox_id
	#if user_type is employee we need to return employee data.
	elif user_type == "employee":
	    new_match_message['user'] ={}
	    new_match_message['user']['firstName'] =  match_message.employer.first_name
	    new_match_message['user']['lastName'] = match_message.employer.last_name
	    new_match_message['user']['socialUserID'] = match_message.employer.social_user_id
	    new_match_message['user']['id'] = match_message.employer.id
	    new_match_message['user']['profilePictureURL'] = match_message.employer.profile_picture_url
	    new_match_message['isUnread'] = match_message.is_new_employee
	    new_match_message['user']['quickbloxID'] = match_message.employer.quickblox_id

	new_match_message['lastMessageSent'] = match_message.last_message_sent.isoformat()
	#check to see if we can truncate the values
	last_match_message = (match_message.last_message_text[:30] + '..') if len(match_message.last_message_text) > 30 else match_message.last_message_text
	new_match_message['dialogID'] = match_message.chatroom_id
	new_match_message['lastMessageText'] = last_match_message
	print new_match_message
	return_match_messages.append(new_match_message)

    #return the match messages
    return HttpResponse(json.dumps(return_match_messages))




#Function to get match chatroom info betweeen two users
def get_chatroom(request):
    try:
	user_id = request.GET.get('socialUserID')
	access_token= request.GET.get('accessToken')
	user_type =  request.GET.get('userType')
	chatroom_id = request.GET.get('chatroomID')
    except:
	raise HttpResponseBadRequest()

    #get user
    user = get_user(user_id, access_token)
    if user == None:
	return HttpResponseForbidden()

    #get all the matches
    try:
	#if user type is employee then grab employees
	if user_type == "employee":
	    match_message = JobMatch.objects.get(employee = user, id = chatroom_id)
	    return_user = match_message.employer
	elif user_type == "employer":
	    match_message = JobMatch.objects.get(employer = user, id = chatroom_id)
	    return_user = match_message.employee
	#else grab employer data
    except:
	return HttpResponseServerError()
    #prepare chatroom for return
    return_chatroom={}
    return_chatroom['user'] = {}
    return_chatroom['user']['firstName'] = return_user.first_name
    return_chatroom['user']['lastName'] = return_user.last_name
    return_chatroom['user']['socialUserID'] = return_user.social_user_id
    return_chatroom['user']['id']=return_user.id
    return_chatroom['user']['profilePictureURL'] = return_user.profile_picture_url
    return_chatroom['user']['quickbloxID'] = return_user.quickblox_id
    return_chatroom['job'] = {}
    return_chatroom['job']['id'] = match_message.job.id
    return_chatroom['job']['title'] = match_message.job.title
    return_chatroom['matchID'] = match_message.id
    return_chatroom['chatroomID'] = match_message.chatroom_id
    return HttpResponse(json.dumps(return_chatroom))



@require_POST
@csrf_exempt
def set_chatroom(request):
    try:
	post_data= json.loads(request.body)
	user_id = post_data.get('socialUserID')
	access_token= post_data.get('accessToken')
	chatroom_id = post_data.get('qbChatoomID')
	match_id = post_data.get('matchID')
	user_type = post_data.get('userType')
    except:
	return HttpResponseBadRequest
    user = get_user(user_id, access_token)
    if user == None:
	return HttpResponseForbidden()
    if user_type == "employee":
	try:
	    job_match = JobMatch.objects.get(id = match_id, employee = user)
	except:
	    return HttpResponseServerError()
    elif user_type == "employer":
	try:
	    job_match = JobMatch.objects.get(id = match_id, employer = user)
	except:
	    return HttpResponseServerError()
    else:
	return HttpResponseForbidden()
    #set job match chatroom id
    job_match.chatroom_id = chatroom_id
    job_match.save()
    return HttpResponse()


@require_POST
@csrf_exempt
def update_chatroom(request):
    try:
	post_data= json.loads(request.body)
	user_id = post_data.get('socialUserID')
	access_token= post_data.get('accessToken')
	chatroom_id = post_data.get('qbChatoomID')
	match_id = post_data.get('matchID')
	last_message = post_data.get('lastMessage')
	user_type = post_data.get('userType')
    except:
	return HttpResponseBadRequest
    user = get_user(user_id, access_token)
    if user == None:
	return HttpResponseForbidden()
    if user_type == "employee":
	try:
	    job_match = JobMatch.objects.get(id = match_id, employee = user)
	    job_match.is_new_employer = True
	except:
	    return HttpResponseServerError()
    elif user_type == "employer":
	try:
	    job_match = JobMatch.objects.get(id = match_id, employer = user)
	    job_match.is_new_employee = True
	except:
	    return HttpResponseServerError()
    else:
	return HttpResponseForbidden()

    job_match.last_message_text = last_message
    job_match.save()
    return HttpResponse()




@require_POST
@csrf_exempt
def mark_as_read(request):
    try:
	post_data= json.loads(request.body)
	user_id = post_data.get('socialUserID')
	access_token= post_data.get('accessToken')
	chatroom_id = post_data.get('qbChatoomID')
	match_id = post_data.get('matchID')
	user_type = post_data.get('userType')
    except:
	return HttpResponseBadRequest
    user = get_user(user_id, access_token)
    if user == None:
	return HttpResponseForbidden()
    if user_type == "employee":
	try:
	    job_match = JobMatch.objects.get(id = match_id, employee = user)
	    job_match.is_new_employee = False
	except:
	    return HttpResponseServerError()
    elif user_type == "employer":
	try:
	    job_match = JobMatch.objects.get(id = match_id, employer = user)
	    job_match.is_new_employer = False
	except:
	    return HttpResponseServerError()
    else:
	return HttpResponseForbidden()

    job_match.save()
    return HttpResponse()


#get jobs that both user has
@require_GET
@csrf_exempt
def get_jobs_chatroom(request):
    try:
	user_id = request.GET.get('userID')
	access_token = request.GET.get('accessToken')
	other_user_id = request.GET.get('otherUserID')
	user_type = request.GET.get('userType')
    except:
	return HttpResponseBadRequest()

    user = get_user(user_id, access_token)
    if user == None:
	return HttpResponseForbidden()
    try:
	if user_type == "employee":
	    employer = UserInfo.objects.get(id = other_user_id)
	    job_matches = JobMatch.objects.filter(employee = user, employer = employer,is_unmatched = False).order_by('-date_added')
	elif user_type == "employer":
	    employee = UserInfo.objects.get(id  =other_user_id)
	    job_matches = JobMatch.objects.filter(employer = user, employee=employee,is_unmatched = False).order_by('-date_added')

    except Exception, e:
	print str(e)
	return HttpResponseServerError()
    #return all the jobs that are matched for the user
    jobs = []
    for job_match in job_matches:
	new_job = {}
	new_job['matchID'] = job_match.id
	new_job['title'] = job_match.job.title
	new_job['isHired'] = job_match.is_hired
	new_job['dateApplied'] = job_match.date_added.isoformat()
	new_job['isHired']=job_match.is_hired
	new_job['id'] = job_match.job.id
	new_job['isOffered'] = job_match.is_offered
	new_job['wage'] = str(job_match.job.wage)
	new_job['otherUser'] = {}
	new_job['isGuaranteePay'] = job_match.job.is_guarantee_pay
	if job_match.job.is_guarantee_pay:
	    new_job['matchType'] = "guarantee"
	else:
	    new_job['matchType']= 'regular'
	new_job['isCompleted'] = job_match.job.is_completed

	if user_type == "employee":
	    new_job['isEmployer'] = False
	    new_job['otherUser']['id'] = job_match.employer.id
	elif user_type == "employer":
	    new_job['isEmployer'] = True
	    new_job['otherUser']['id'] = job_match.employee.id
	jobs.append(new_job)

    return HttpResponse(json.dumps(jobs))
